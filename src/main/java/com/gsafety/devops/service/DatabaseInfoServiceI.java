package com.gsafety.devops.service;
import org.jeecgframework.core.common.service.CommonService;

import com.gsafety.devops.entity.DatabaseInfoEntity;

import java.io.Serializable;

public interface DatabaseInfoServiceI extends CommonService{
	
 	public void delete(DatabaseInfoEntity entity) throws Exception;
 	
 	public Serializable save(DatabaseInfoEntity entity) throws Exception;
 	
 	public void saveOrUpdate(DatabaseInfoEntity entity) throws Exception;
 	
}
