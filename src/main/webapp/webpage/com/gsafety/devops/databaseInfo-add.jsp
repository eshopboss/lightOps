<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>数据库信息</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<t:base type="bootstrap,layer,validform,bootstrap-form"></t:base>
</head>
 <body style="overflow:hidden;overflow-y:auto;">
 <div class="container" style="width:100%;">
	<div class="panel-heading"></div>
	<div class="panel-body">
	<form class="form-horizontal" role="form" id="formobj" action="databaseInfoController.do?doAdd" method="POST">
		<input type="hidden" id="btn_sub" class="btn_sub"/>
		<input type="hidden" id="id" name="id"/>
		<div class="form-group">
			<label for="dbDesc" class="col-sm-3 control-label">数据库描述：</label>
			<div class="col-sm-7">
				<div class="input-group" style="width:100%">
					<input id="dbDesc" name="dbDesc" type="text" maxlength="100" class="form-control input-sm" placeholder="请输入数据库描述"  ignore="ignore" />
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label for="dbCode" class="col-sm-3 control-label">数据库标识：</label>
			<div class="col-sm-7">
				<div class="input-group" style="width:100%">
					<input id="dbCode" name="dbCode" type="text" maxlength="32" class="form-control input-sm" placeholder="请输入数据库标识"  ignore="ignore" />
				</div>
			</div>
		</div>
		<%--
		<div class="form-group">
			<label for="dbAddr" class="col-sm-3 control-label">数据库IP：</label>
			<div class="col-sm-7">
				<div class="input-group" style="width:100%">
					<input id="dbAddr" name="dbAddr" type="text" maxlength="32" class="form-control input-sm" placeholder="请输入数据库IP"  ignore="ignore" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="dbPort" class="col-sm-3 control-label">数据库端口：</label>
			<div class="col-sm-7">
				<div class="input-group" style="width:100%">
					<input id="dbPort" name="dbPort" type="text" maxlength="32" class="form-control input-sm" placeholder="请输入数据库端口"  ignore="ignore" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="dbType" class="col-sm-3 control-label">数据库类型：</label>
			<div class="col-sm-7">
				<div class="input-group" style="width:100%">
					<!-- <input id="dbType" name="dbType" type="text" maxlength="32" class="form-control input-sm" placeholder="请输入数据库类型"  ignore="ignore" /> -->
					<t:dictSelect field="dbType" type="radio" typeGroupCode="db_type" hasLabel="false" title="数据库类型" defaultVal="MySQL"></t:dictSelect>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="dbName" class="col-sm-3 control-label">数据库名称：</label>
			<div class="col-sm-7">
				<div class="input-group" style="width:100%">
					<input id="dbName" name="dbName" type="text" maxlength="32" class="form-control input-sm" placeholder="请输入数据库名称"  ignore="ignore" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="dbUser" class="col-sm-3 control-label">数据库账号：</label>
			<div class="col-sm-7">
				<div class="input-group" style="width:100%">
					<input id="dbUser" name="dbUser" type="text" maxlength="32" class="form-control input-sm" placeholder="请输入数据库账号"  ignore="ignore" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<label for="dbPwd" class="col-sm-3 control-label">数据库密码：</label>
			<div class="col-sm-7">
				<div class="input-group" style="width:100%">
					<input id="dbPwd" name="dbPwd" type="text" maxlength="32" class="form-control input-sm" placeholder="请输入数据库密码"  ignore="ignore" />
				</div>
			</div>
		</div>
		 --%>
	</form>
	</div>
 </div>
<script type="text/javascript">
var subDlgIndex = '';
$(document).ready(function() {
	//单选框/多选框初始化
	$('.i-checks').iCheck({
		labelHover : false,
		cursor : true,
		checkboxClass : 'icheckbox_square-green',
		radioClass : 'iradio_square-green',
		increaseArea : '20%'
	});
	
	//表单提交
	$("#formobj").Validform({
		tiptype:function(msg,o,cssctl){
			if(o.type==3){
				validationMessage(o.obj,msg);
			}else{
				removeMessage(o.obj);
			}
		},
		btnSubmit : "#btn_sub",
		btnReset : "#btn_reset",
		ajaxPost : true,
		beforeSubmit : function(curform) {
		},
		usePlugin : {
			passwordstrength : {
				minLen : 6,
				maxLen : 18,
				trigger : function(obj, error) {
					if (error) {
						obj.parent().next().find(".Validform_checktip").show();
						obj.find(".passwordStrength").hide();
					} else {
						$(".passwordStrength").show();
						obj.parent().next().find(".Validform_checktip").hide();
					}
				}
			}
		},
		callback : function(data) {
			var win = frameElement.api.opener;
			if (data.success == true) {
				frameElement.api.close();
			    win.reloadTable();
			    win.tip(data.msg);
			} else {
			    if (data.responseText == '' || data.responseText == undefined) {
			        $.messager.alert('错误', data.msg);
			        $.Hidemsg();
			    } else {
			        try {
			            var emsg = data.responseText.substring(data.responseText.indexOf('错误描述'), data.responseText.indexOf('错误信息'));
			            $.messager.alert('错误', emsg);
			            $.Hidemsg();
			        } catch (ex) {
			            $.messager.alert('错误', data.responseText + "");
			            $.Hidemsg();
			        }
			    }
			    return false;
			}
		}
	});
		
});
</script>
</body>
</html>