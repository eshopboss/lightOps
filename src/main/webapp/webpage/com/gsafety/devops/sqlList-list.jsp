<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<link rel="stylesheet" href="${webRoot}/plug-in/mutitables/datagrid.menu.css" type="text/css"></link>
<style>.datagrid-toolbar{padding:0 !important;border:0}</style>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:0px;border:0px">
  <t:datagrid name="sqlListList" filterBtn="true" complexSuperQuery="sqlList" checkbox="true" fitColumns="false" title="" sortName="createDate" sortOrder="desc" actionUrl="sqlListController.do?datagrid" idField="id" fit="true" pageSize="20" queryMode="group" onDblClick="datagridDbclick" extendParams="checkOnSelect:false,onSelect:function(index,row){datagridSelect(index,row);}" onLoadSuccess="optsMenuToggle">
   <t:dgCol title="操作" field="opt" width="77" optsMenu="true"></t:dgCol>
   <t:dgFunOpt funname="curd.addRow" urlfont="fa-plus" title="新增一行" />
   <t:dgFunOpt funname="curd.deleteOne(id)" urlfont="fa-minus" title="删除该行" />
   <t:dgFunOpt inGroup="true" funname="curd.detail(id)" urlfont="fa-eye" title="查看详情"/>
   <t:dgFunOpt inGroup="true" exp="bpmStatus#eq#1"  funname="startProcess(id)" urlfont="fa-level-up" title="提交流程"/>
   <t:dgCol title="主键"  field="id"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="创建日期"  field="createDate"  formatter="yyyy-MM-dd"  filterType="datebox" extendParams="editor:{type:'datebox',options:{onShowPanel:initDateboxformat}}" width="120"></t:dgCol>
   <t:dgCol title="创建人名称"  field="createName"  hidden="false"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="SQL描述"  field="sqlName"  extendParams="editor:'text'" width="220"></t:dgCol>
   <t:dgCol title="SQL内容"  field="sqlContent"  extendParams="editor:'text'" width="300"></t:dgCol>
   <t:dgCol title="SQL类型"  field="sqlType"  dictionary="sql_type" filterType="combobox" extendParams="editor:'combobox'" width="120"></t:dgCol>
   <t:dgCol title="创建人登录名称"  field="createBy"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="更新人名称"  field="updateName"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="更新人登录名称"  field="updateBy"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="更新日期"  field="updateDate"  formatter="yyyy-MM-dd"  filterType="datebox" extendParams="editor:{type:'datebox',options:{onShowPanel:initDateboxformat}}" width="120"></t:dgCol>
   <t:dgCol title="所属部门"  field="sysOrgCode"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="所属公司"  field="sysCompanyCode"  hidden="true"  extendParams="editor:'text'" width="120"></t:dgCol>
   <t:dgCol title="流程状态"  field="bpmStatus"  hidden="true"  dictionary="bpm_status" filterType="combobox" extendParams="editor:'combobox'" width="120"></t:dgCol>
  </t:datagrid>
  </div>
 </div>
  <script type="text/javascript" src="${webRoot}/plug-in/mutitables/mutitables.urd.js"></script>
  <script type="text/javascript" src="${webRoot}/plug-in/mutitables/mutitables.curdInIframe.js"></script>
  <script type="text/javascript">
 $(function(){
	  curd = $.curdInIframe({
		  name:"sqlList",
		  isMain:true,
		  queryCode:"sqlList",
		  describe:"SQL列表",
		  form:{width:'100%',height:'100%'},
		  urls:{
			  excelImport:'sqlListController.do?mainUpload'
		  }
	  });
	  gridname = curd.getGridname();
 });
 
 /**
  * 选中事件加载子表数据
  */
 function datagridSelect(index,row){
	$('#sqlListList').datagrid('unselectAll');
 	parent.initSubList(row.id);
 }

 /**
  * 主页面重置调用方法
  */
 function queryResetit(){
	searchReset('sqlListList');
	sqlListListsearch();
 }
 
 /**
  * 刷新子表数据
  */
 function refreshSubTable(){
	 parent.freshSubList();
 }
 
 /**
  * 双击事件开始编辑
  */
 function datagridDbclick(index,field,value){
 	$("#sqlListList").datagrid('beginEdit', index);
 }
 
 /**
  * dataGrid 操作菜单 切换显示隐藏  datagrid加载成功之后执行该方法*/
function optsMenuToggle(data){
	optsMenuToggleBydg($("#sqlListList").datagrid('getPanel'));
}
//模板下载
function ExportXlsByT() {
	JeecgExcelExport("sqlListController.do?exportXlsByT","sqlListList");
}
//导出
function ExportXls() {
	JeecgExcelExport("sqlListController.do?exportXls","sqlListList");
}

function startProcess(id){
	 //业务表名
	var tableName = "sql_list"; 
	var formUrl = "sqlListController.do?goUpdate&load=detail";
	var formUrlMobile = "sqlListController.do?goUpdate&load=detail";
	
	var url = "activitiController.do?startUserDefinedProcess&id="+id
			   +"&tableName="+tableName
			   +"&formUrl="+formUrl
			   +"&formUrlMobile="+formUrlMobile;
	 confirm(url,'确定提交流程吗？','sqlListList');
	 
 }
 </script>